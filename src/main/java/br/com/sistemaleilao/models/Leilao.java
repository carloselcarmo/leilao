package br.com.sistemaleilao.models;

import java.util.ArrayList;
import java.util.List;

public class Leilao {
    private List<Lance> lances;

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public Leilao()
    {
        lances = new ArrayList<>();
    }

    public void adicionarNovoLance(Lance lance)
    {
        if(validarNovoLance(lance))
        {
            lances.add(lance);
        }
    }

    private boolean validarNovoLance(Lance lance)
    {
        if(lances.size() == 0)
        {
            return true;
        }
        else
        {
            if(lance.getValor() > lances.get((lances.size() - 1)).getValor())
            {
                return true;
            }
            else
            {
                throw new RuntimeException("O lance deve ser maior que o último lance");
            }
        }
    }
}
