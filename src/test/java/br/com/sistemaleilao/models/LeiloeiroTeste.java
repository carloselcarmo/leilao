package br.com.sistemaleilao.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LeiloeiroTeste
{
    @Test
    public void TestarMaiorLance()
    {
        //Preparação
        Usuario primeiroUsuario = new Usuario(1, "João");
        Usuario segundoUsuario = new Usuario(2, "Maria");
        Lance primeiroLance = new Lance(primeiroUsuario, 10.0);
        Lance segundoLance = new Lance(segundoUsuario, 20.0);
        Lance terceiroLance = new Lance(primeiroUsuario, 50.0);
        Leilao leilao = new Leilao();
        Leiloeiro leiloeiro = new Leiloeiro("Zé", leilao);

        //Execução
        leilao.adicionarNovoLance(primeiroLance);
        leilao.adicionarNovoLance(segundoLance);
        leilao.adicionarNovoLance(terceiroLance);
        Lance maiorLance = leiloeiro.retornarMaiorLance();

        //Verificação
        Assertions.assertEquals(primeiroUsuario.getNome(), maiorLance.getUsuario().getNome());
        Assertions.assertEquals(terceiroLance.getValor(), maiorLance.getValor());
    }

    @Test
    public void TestarMaiorLanceComLeilaoSemLances()
    {
        //Preparação
        Usuario Usuario = new Usuario(1, "João");
        Leilao leilao = new Leilao();
        Leiloeiro leiloeiro = new Leiloeiro("Zé", leilao);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {Lance maiorLance = leiloeiro.retornarMaiorLance();});
    }
}
