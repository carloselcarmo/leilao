package br.com.sistemaleilao.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LeilaoTeste
{
    @Test
    public void TestarAdicionarNovoLance()
    {
        //Preparação
        Usuario usuario = new Usuario(1, "João");
        Lance lance = new Lance(usuario, 10.0);
        Leilao leilao = new Leilao();

        Integer quantidadeLancesEsperada = 1;

        //Execução
        leilao.adicionarNovoLance(lance);

        //Verificação
        Assertions.assertEquals(quantidadeLancesEsperada, leilao.getLances().size());
        Assertions.assertEquals(usuario.getNome(), leilao.getLances().get(0).getUsuario().getNome());
        Assertions.assertEquals(lance.getValor(), leilao.getLances().get(0).getValor());
    }

    @Test
    public void TestarInclusaoDeDoisLances()
    {
        //Preparação
        Usuario usuario = new Usuario(1, "João");
        Lance primeiroLance = new Lance(usuario, 10.0);
        Lance segundoLance = new Lance(usuario, 20.0);
        Leilao leilao = new Leilao();

        Integer quantidadeLancesEsperada = 2;

        //Execução
        leilao.adicionarNovoLance(primeiroLance);
        leilao.adicionarNovoLance(segundoLance);

        //Verificação
        Assertions.assertEquals(quantidadeLancesEsperada, leilao.getLances().size());
    }

    @Test
    public void TestarInclusaoDeLanceComMenorValor()
    {
        //Preparação
        Usuario usuario = new Usuario(1, "João");
        Lance primeiroLance = new Lance(usuario, 10.0);
        Lance segundoLance = new Lance(usuario, 5.0);
        Leilao leilao = new Leilao();

        //Execução
        leilao.adicionarNovoLance(primeiroLance);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {leilao.adicionarNovoLance(segundoLance);});
    }
}
