package br.com.sistemaleilao.models;

import java.util.List;

public class Leiloeiro
{
    private String nome;
    private Leilao leilao;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeiloes() {
        return leilao;
    }

    public void setLeiloes(Leilao leilao) {
        this.leilao = leilao;
    }

    public Leiloeiro() {
    }

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public Lance retornarMaiorLance()
    {
        if(leilao.getLances().size() > 0)
        {
            return leilao.getLances().get(leilao.getLances().size() - 1);
        }
        else
        {
            throw new RuntimeException("Não há lances no leilão");
        }
    }
}
